import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, Store, StoreFeatureModule as NgRxStoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import Dexie from 'dexie';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component'; // ANGULAR CLI environment
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { EspiameDirective } from './espiame.directive';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { DestinoViaje } from './models/destino-viaje.model';
import {
  DestinosViajesEffects, DestinosViajesState,

  initializeDestinosViajesState,
  InitMyDataAction, reducerDestinosViajes
} from './models/destinos-viajes-state.model';
import { ReservasModule } from './reservas/reservas.module';
import { AuthService } from './services/auth.service';
import { TrackearClickDirective } from './trackear-click.directive';


//app config
export interface AppConfig{
  apiEndpoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

//fin app config

//routin init

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch:'full'},
  { path: 'main', component: VuelosMainComponentComponent},
  { path: 'mas-info', component: VuelosMasInfoComponentComponent},
  { path: ':id', component: VuelosDetalleComponentComponent}


];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch:'full'},
  { path: 'home', component: ListaDestinosComponent},
  { path: 'destino/:id', component: DestinoDetalleComponent},
  { path: 'login', component: LoginComponent},
  { path: 'protected',
   component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard]
},
{ 
  path: 'vuelos',
  component: VuelosComponentComponent,
  canActivate: [UsuarioLogueadoGuard],
  children: childrenRoutesVuelos
}

];
  
// redux init
//Definimos el estado global de la app

export interface AppState{
  destinos: DestinosViajesState;
}

// constantes de los reducers
const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

// Inicializacion de los reducers
const reducersInitialState = {
  destinos: initializeDestinosViajesState()
};

// redux fin init

//app init 

export function init_app(appLoadService : AppLoadService) : () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async initializeDestinosViajesState(): Promise<any> {
    const headers : HttpHeaders = new HttpHeaders({'X-API-TOKEN' : 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers: headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

//fin app init

//dexie db
export class Translation{
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable ({
  providedIn: 'root'
})

export class MyDatabae extends Dexie{
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',
    });
    this.version(2).stores({
      destinos: '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabae();
//fin dexie db

// i18n ini
class TranslationLoader implements TranslateLoader{
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any>{
    const promise = db.translations
    .where('lang')
    .equals(lang)
    .toArray()
    .then(results => {
      if(results.length === 0){
        return.http
        .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
        .toPromise()
        .then(apiResults => {
          db.translations.bulkAdd(apiResults);
          return apiResults;
        });
      }
      return results;
    }).then((traducciones) =>{
      console.log('traducciones cargadas:');
      console.log(traducciones);
      return traducciones;
    }).then((traducciones) => {
      return traducciones.map((t) => ({ [t.key]: t.value}));
    });
    return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
//fin i18n


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    NgRxStoreModule,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService, UsuarioLogueadoGuard, 
    { provide : APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide : APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

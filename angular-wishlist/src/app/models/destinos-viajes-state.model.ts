// IMPLANTACION DE REDUX
import { Injectable } from '@angular/core';
import { Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';


// ESTADO
export interface DestinosViajesState {
  items: DestinoViaje[];
  loading: boolean;
  favorito: DestinoViaje;
}

export function initializeDestinosViajesState() {
  return {
    items: [],
    loading: false,
    favorito: null
  }
}


// ACCIONES
export enum DestinosViajesActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  VOTE_UP = '[Destinos Viajes] Vote Up',
  VOTE_DOWN = '[Destinos Viajes] Vote Down',
  INIT_MY_DATA = '[Destinos Viajes] Init My Data'
  
}

export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
  type = DestinosViajesActionTypes.VOTE_UP;
  constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
  type = DestinosViajesActionTypes.VOTE_DOWN;
  constructor(public destino: DestinoViaje) {}
}

export class InitMyDataAction implements Action{
  type = DestinosViajesActionTypes.INIT_MY_DATA;
  constructor(public destinos: string[]) {}
}


export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction | InitMyDataAction;

// REDUCERS - son llamados uno a uno en el orden en el que aparecen cada vez que// salta una accion, cada reducer recibe el estado anterior del sistema y recibe// la accion que se esta disparando, y en funcion de switch que le dice lo que t// iene que hacer modifica el estado y en caso de que no coincida con ninguna ac// cion de los CASE devuelve el estado sin cambiar

export function reducerDestinosViajes (
  state: DestinosViajesState,
  action: DestinosViajesActions
) : DestinosViajesState {
   switch(action.type) {
    case DestinosViajesActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).destinos;
      return{
        ...state,
        items: destinos.map((d) => new DestinoViaje(d,''))
      };
    }
    
     case DestinosViajesActionTypes.NUEVO_DESTINO: {
       return {
         ...state,
	 items: [...state.items, (action as NuevoDestinoAction).destino]
       };
     }

     case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
       state.items.forEach(x => x.setSelected(false));
       const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
       fav.setSelected(true);
       return {
         ...state,
         favorito : fav
       };
     }

   case DestinosViajesActionTypes.VOTE_UP: {
     const d: DestinoViaje = (action as VoteUpAction).destino;
     d.voteUp();
     return { ...state };
   }

    case DestinosViajesActionTypes.VOTE_DOWN: {
     const d: DestinoViaje = (action as VoteDownAction).destino;
     d.voteDown();
     return { ...state };
    }
  }
};

// EFFECTS -  esa accion es pasada a todos los effects registrados en el sistema. El objetivo de los effects es registrar una nueva acción como consecuencia de otra acción. En nuestro caso convierte el nuevo destino en FAVORITO

@Injectable()
export class DestinosViajesEffects{
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe (
    ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
    map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino)));
    

constructor(private actions$: Action) {}
}